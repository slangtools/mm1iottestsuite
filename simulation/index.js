'use strict';

const server = require('http').Server();
const io = require('socket.io')(server);
const config = require('../config/config');
const SOCKET_EVENT_TYPES = require('../config/socketEventTypes');

let deviceList = {
  'bulb0': {
    "timestamp": 0,
    "deviceUid": 'bulb0',
    "eventType": 0,
    "direction": 1,
    "payload": 0
  },
  'bulb1': {
    "timestamp": 0,
    "deviceUid": 'bulb1',
    "eventType": 0,
    "direction": 1,
    "payload": 1
  },
  'bulb2': {
    "timestamp": 0,
    "deviceUid": 'bulb2',
    "eventType": 0,
    "direction": 1,
    "payload": 0
  },
  'door0': {
    "timestamp": 0,
    "deviceUid": 'door0',
    "eventType": 0,
    "direction": 1,
    "payload": 0
  },
  'door1': {
    "timestamp": 0,
    "deviceUid": 'door1',
    "eventType": 0,
    "direction": 1,
    "payload": 0
  },
  'door2': {
    "timestamp": 0,
    "deviceUid": 'door2',
    "eventType": 0,
    "direction": 1,
    "payload": 0
  }
};

let dictionaryActionsByDeviceUid = {
  'bulb0': (data) => {
      let deviceData = deviceList[data.deviceUid];
      deviceData.timestamp = new Date().getTime();
      if(data.eventType === 1) {
        console.log(deviceData);
        deviceData.payload = 1
      }
      return deviceData;
    },
  'bulb1': (data) => {
      let deviceData = deviceList[data.deviceUid];
      deviceData.timestamp = new Date().getTime();
      if(data.eventType === 1) {
        console.log(deviceData);
        deviceData.payload = 1
      } else if(data.eventType === 0) {
        console.log(deviceData);
        deviceData.payload = 0
      }
      return deviceData;
    },
  'bulb2': (data) => {
      let deviceData = deviceList[data.deviceUid];
      deviceData.timestamp = new Date().getTime();
      if(data.eventType === 1) {
        console.log(deviceData);
        deviceData.payload = 1
      }
      return deviceData;
    },
  'door0': (data) => {
      let deviceData = deviceList[data.deviceUid];
      deviceData.timestamp = new Date().getTime();
      if(data.eventType === 1) {
        console.log(deviceData);
        deviceData.payload = 1
      }
      return deviceData;
    },
  'door1': (data) => {
      let deviceData = deviceList[data.deviceUid];
      deviceData.timestamp = new Date().getTime();
      if(data.eventType === 1) {
        console.log(deviceData);
        deviceData.payload = 1
      }
      return deviceData;
    },
  'door2': (data) => {
      let deviceData = deviceList[data.deviceUid];
      deviceData.timestamp = new Date().getTime();
      if(data.eventType === 1) {
        console.log(deviceData);
        deviceData.payload = 1
      }
      return deviceData;
    },
  'default': () => {
      console.log('DEVICEUID NOT FOUND');
      return {title: 'device not found'};
    },
};

io.on('connection', (socket) => {
  console.log('transport connected');

  socket.on(SOCKET_EVENT_TYPES.lowerSendToDevice, (data) => {
    console.log('On lowerSendToDevice');

    let deviceUid = data.deviceUid;

    let responseData = deviceUid in dictionaryActionsByDeviceUid ? dictionaryActionsByDeviceUid[deviceUid](data) : dictionaryActionsByDeviceUid.default();
    socket.emit(SOCKET_EVENT_TYPES.lowerSendToDeviceResponse, responseData);
  });


  socket.on('disconnect', () => {
    console.log('transport disconnected');
  });
});

server.listen(config.port, () => {
  console.log('Simulation server running at port ' + config.port);
});
