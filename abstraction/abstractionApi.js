'use strict';

const EventEmitter = require('events').EventEmitter;
const EVENT_TYPES = require('../config/eventTypes');

class AbstractionApi extends EventEmitter {
  constructor(transport) {
    super();
    this._transport = transport;
  }
  init() {
    let self = this;
    return this._transport.init()
    .then(() => {
      return self._transport.on(EVENT_TYPES.lowerReceivedFromDevice, (deviceMessage) => {
        self.emit(EVENT_TYPES.upperReceiveFromDevice, deviceMessage);
      });
    });

  }
  upperSendToDevice(params) {
    return this._transport.lowerSendToDevice(params);
  }
}

module.exports = AbstractionApi;
