'use strict';

var expect = require("chai").expect;
const Promise = require('bluebird');
var TransportApi = require('../transport/transportApi');
var AbstractionApi = require('../abstraction/abstractionApi');
var EVENT_TYPES = require('../config/eventTypes');

var transport = new TransportApi();
var abstraction = new AbstractionApi(transport);

var message = {
  "timestamp": 0,
  "deviceUid": 0,
  "eventType": 0,
  "direction": 0,
  "payload": 0
};

describe("Check", function () {
  before(function () {
     abstraction.init();
  });

  it('"bulb0" payload to 1', function (done) {
    var data = Object.assign({}, message);
    data.deviceUid = 'bulb0';
    data.timestamp = new Date().getTime();
    data.eventType = 1;
    abstraction.upperSendToDevice(data);
    abstraction.once(EVENT_TYPES.upperReceiveFromDevice, function(data) {
      console.log('ON -> ', data);
      expect(data.payload).to.equal(1);
      done();
    });
  });

  it('"bulb1" payload to 0', function (done) {
    var data = Object.assign({}, message);
    data.deviceUid = 'bulb1';
    data.timestamp = new Date().getTime();
    data.eventType = 0;
    abstraction.upperSendToDevice(data);
    abstraction.once(EVENT_TYPES.upperReceiveFromDevice, function(data) {
      console.log('ON -> ', data);
      expect(data.payload).to.equal(0);
      done();
    });
  });

  it('"bulb2" payload to 1', function (done) {
    var data = Object.assign({}, message);
    data.deviceUid = 'bulb2';
    data.timestamp = new Date().getTime();
    data.eventType = 1;
    abstraction.upperSendToDevice(data);
    abstraction.once(EVENT_TYPES.upperReceiveFromDevice, function(data) {
      console.log('ON -> ', data);
      expect(data.payload).to.equal(1);
      done();
    });
  });

});
