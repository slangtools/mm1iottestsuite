'use strict';

var expect = require("chai").expect;
var TransportApi = require('../transport/transportApi');
var EVENT_TYPES = require('../config/eventTypes');

var transport = new TransportApi();

var message = {
  "timestamp": 0,
  "deviceUid": 0,
  "eventType": 0,
  "direction": 0,
  "payload": 0
};

describe("Check", function () {
  before(function () {
     transport.init();
  });
  it('"bulb0" payload to 1', function (done) {
    var data = Object.assign({}, message);
    data.deviceUid = 'bulb0';
    data.timestamp = new Date().getTime();
    data.eventType = 1;
    transport.lowerSendToDevice(data);
    transport.once(EVENT_TYPES.lowerReceivedFromDevice, function(data) {
      console.log('ON -> ', data);
      expect(data.payload).to.equal(1);
      done();
    });
  });

  it('"door" payload to 1', function (done) {
    var data = Object.assign({}, message);
    data.deviceUid = 'door0';
    data.timestamp = new Date().getTime();
    data.eventType = 1;
    transport.lowerSendToDevice(data);
    transport.once(EVENT_TYPES.lowerReceivedFromDevice, function(data) {
      console.log('ON -> ', data);
      expect(data.payload).to.equal(1);
      done();
    });
  });

});
