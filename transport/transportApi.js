'use strict';

const EventEmitter = require('events').EventEmitter;
const Promise = require('bluebird');
const io = require('socket.io-client');
const config = require('../config/config');
const EVENT_TYPES = require('../config/eventTypes');
const SOCKET_EVENT_TYPES = require('../config/socketEventTypes');

class TransportApi extends EventEmitter {
  constructor() {
    super();
  }
  init() {
    console.log('Init >>>');
    let self = this;
    return new Promise((resolve, reject) => {
      self.socket = io.connect(config.host);
      resolve(self.socket);
    }).then(() => {
      return self.socket.on(SOCKET_EVENT_TYPES.lowerSendToDeviceResponse, (data) => {
        self.emit(EVENT_TYPES.lowerReceivedFromDevice, data);
      });
    });
  }
  lowerSendToDevice(data) {
    console.log('lowerSendToDevice >>>');
    let self = this;
    return new Promise((resolve, reject) => {
      resolve(self.socket.emit(SOCKET_EVENT_TYPES.lowerSendToDevice, data));
    });
  }
}

module.exports = TransportApi;
